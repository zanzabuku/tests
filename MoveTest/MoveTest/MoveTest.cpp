// MoveTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <iostream>
#include <vector>

class MoveTest {
public:
	MoveTest() {

	}

	MoveTest(MoveTest& other) {
		std::cout << "copy constructor" << std::endl;
	}

	MoveTest(MoveTest&& other) {
		std::cout << "move constructor" << std::endl;
	}

	MoveTest& operator =(const MoveTest& other) {
		std::cout << "operator =" << std::endl;
		return *this;
	}
};

std::vector<MoveTest> getVector() {
	std::vector<MoveTest> objects;
	objects.reserve(10);
	objects.push_back(MoveTest());
	objects.push_back(MoveTest());
	objects.push_back(MoveTest());

	return objects;
}

void getVector2(std::vector<MoveTest>& v) {
	// here move is needed
	std::vector<MoveTest> os = getVector();
	v = std::move(os);
}

void getVector3(std::vector<MoveTest>& v) {
	// here move is not needed
	v = std::move(getVector());
}

int main()
{
	std::vector<MoveTest> os;
	getVector2(os);
	return 0;
}

